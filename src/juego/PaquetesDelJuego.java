package juego;
import entorno.Entorno;

public class PaquetesDelJuego 
{
	public Paquete[] paquetes;
	private boolean[] paquetesVisibles;
	
	
	
	public PaquetesDelJuego(Pared p)
	{
		paquetes = new Paquete[p.CantidadDePaquetes()];
		paquetesVisibles = new boolean[p.CantidadDePaquetes()];
		int j = 0;
		
		for (int i = 0; i < p.muro.length; i++)
		{
			if (p.muro[i].getTienePaquete())
			{
				paquetes[j] = new Paquete(p.muro[i]);
				paquetesVisibles[j] = false;
				j++;
			}
		}
	}
	
	//Metodos publicos
	
	public void mover(Entorno ent)
	{
		for (int i = 0; i < this.paquetes.length; i++)
			if (this.paquetesVisibles[i])
				paquetes[i].mover(ent);
	}
	
	public void verificarVisibles(Pared p)
	{
		for(int i = 0; i < this.paquetes.length; i++)
		{
			for(int j = 0; j < p.muro.length; j++)
			{
				if (this.paquetes[i].getX() == p.muro[j].getX() && this.paquetes[i].getY() == p.muro[j].getY() && p.ladrilloVisible[j] == false && p.muro[j].getTienePaquete())
					this.paquetesVisibles[i] = true;
			}
		}
	}
	
	public void colisionConBarra(Barrita barra)
	{
		int borde = 1;
		for (int i = 0; i < this.paquetes.length; i++)
		{
			if(this.paquetes[i].getX() >= barra.getVerticeIzquierdo() && this.paquetes[i].getX() <= barra.getVerticeDerecho() && this.paquetes[i].getY() >= barra.getTecho() - borde && this.paquetes[i].getY() <= barra.getTecho() + borde)
			{
				this.paquetesVisibles[i] = false;
				this.paquetes[i].setPaqueteAgarrado(true);

			}
			else if(this.paquetes[i].getY() > barra.getTecho() && this.paquetes[i].getY() <= barra.getPiso() && this.paquetes[i].getX() > barra.getVerticeIzquierdo() && this.paquetes[i].getX() < barra.getVerticeDerecho())
			{
				this.paquetesVisibles[i] = false;
				this.paquetes[i].setPaqueteAgarrado(true);
			}				
		}
	}
	
	public void verificarPoder(Barrita barra, Bola bola, Jugador jugador)
	{
		for (int i = 0; i < paquetes.length; i++)
		{
			if (paquetes[i].getPaqueteAgarrado() && paquetesVisibles[i]==false && paquetes[i].getPaqueteusado()==false)
			{
				paquetes[i].getPoder(bola, barra,jugador);
				paquetes[i].setPaqueteUsado(true);
			}
		}
	}
	
	public void mostrar(Entorno ent)
	{
		for (int i=0; i < this.paquetes.length; i++)
		{
			if (paquetesVisibles[i])
				paquetes[i].mostrar(ent);
		}
	}
		
	public void borrarPaquetesEnPantalla()
	{
		for (int i=0; i < this.paquetes.length; i++)
		{
			if (paquetesVisibles[i])
			{
				paquetes[i].setPaqueteAgarrado(true);
				paquetes[i].setPaqueteUsado(true);
				paquetesVisibles[i] = false;
			}
		}
	}	
}
