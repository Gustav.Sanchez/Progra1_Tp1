package juego;
import entorno.Entorno;

public class Pared 
{
	public Ladrillo [] muro;
	public boolean [] ladrilloVisible; 
	
	public Pared ()
	{
		int cantLadrillos = 143;
		int x = 25;
		int y = 20;
		int anchoDelJuego = 525;
		
		muro = new Ladrillo[cantLadrillos];
		ladrilloVisible = new boolean[cantLadrillos];
		
		muro[0] = new Ladrillo(x,y); 					//Construyo el primer ladrillo de la pared
		ladrilloVisible[0] = true;
		
		int medioLadrillo = muro[0].getAncho()/2;
		
		for (int i = 1; i < muro.length; i++) 
		{
			if (x + medioLadrillo > anchoDelJuego)
			{
				x = 0;
				while (superpuestoY(muro[i-1],y))
					y++;
			}
			
			while (superpuestoX(muro[i-1],x))
				x++;
			
			muro[i] = new Ladrillo(x + medioLadrillo,y);
			ladrilloVisible[i] = true;
		}
		
		//crea la ultima fila de ladrillos chetos
		
		muro[muro.length-1].ladrilloCheto();
			
		for (int i = muro.length - 1; (muro[i].getX() - medioLadrillo) > 0; i-- )
			muro[i-1].ladrilloCheto();		
	}
	
	//Metodos publicos
	
	public int cantidadDeLadrillosVisibles()
	{
		int cant = 0;
		for(int i=0; i<this.ladrilloVisible.length;i++)
		{
			if(ladrilloVisible[i])
				cant++;
		}
		return cant;
	}
	
	public int CantidadDePaquetes()
	{
		int res = 0;
		
		for (int i = 0; i < this.muro.length;i++)
		{
			
			if (muro[i].getTienePaquete())
				res++;		
		}
		return res;
	}
	
	public void verificarColision(Bola x)
	{	
		for (int i = 0; i < muro.length; i++)
		{
			if(muro[i].getVidas() == 0)
				ladrilloVisible[i] = false;
			
			else
				muro[i].colisionConBola(x);	
		}
	}
	
	public void mostrar(Entorno ent)
	{
		for (int j = 0; j < muro.length; j++)
		{	
			if (ladrilloVisible[j])
				muro[j].mostrar(ent);
		}
	}
	
	//Metodos Privados
	
	private boolean superpuestoX(Ladrillo ladrillo, int x)
	{
		return x >= ladrillo.getVerticeIzquierdo() && x <= ladrillo.getVerticeDerecho(); 
	}
	
	private boolean superpuestoY(Ladrillo ladrillo, int y)
	{
		return y >= ladrillo.getTecho() && y <= ladrillo.getPiso(); 
	}
}
