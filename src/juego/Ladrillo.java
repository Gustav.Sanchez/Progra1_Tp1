package juego;
import entorno.Entorno;

import java.awt.Image;
import java.util.Random;
import entorno.Herramientas;

public class Ladrillo 
{
	private int x,y,alto,ancho,vidas;
	private boolean tienePaquete;
	private Image ladrillo;
	
	public Ladrillo(int x, int y)
	{	
		this.x = x;
		this.y = y;
		this.alto = 20;
		this.ancho = 50;
		this.ladrillo = Herramientas.cargarImagen("ladrillo_fondo_transparente.png");
		this.vidas = 1;
		this.tienePaquete = azar();
	}
	
	//Geters
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public int getAlto()
	{
		return this.alto;
	}
	
	public int getAncho()
	{
		return this.ancho;
	}
	
	public int getVerticeIzquierdo()
	{
		return this.getX()-(this.getAncho()/2);
	}
	
	public int getVerticeDerecho()
	{
		return this.getX()+(this.getAncho()/2);
	}
	
	public int getTecho()
	{
		return this.getY() - this.getAlto();
	}
	
	public int getPiso()
	{
		return this.getY() + this.getAlto();
	}
	
	public int getVidas()
	{
		return this.vidas;
	}
	
	public boolean getTienePaquete()
	{
		return this.tienePaquete;
	}
	
	//Metodos publicos
	
	public void ladrilloCheto()
	{
		this.vidas = 2;
		this.ladrillo = Herramientas.cargarImagen("ladrillo_duro_fondo_transparente.png");
	}
	
	public void colisionConBola(Bola bola)
	{
		if (this.contactoEnTecho(bola))  
		{	 
			bola.rebotarVertical();
			vidas--;
			this.LadrilloRoto();
		}

		else if (this.contactoEnPiso(bola))  
		{	 
			bola.rebotarVertical();
			vidas--;
			this.LadrilloRoto();
		}
		
		else if (this.contactoLatIzq(bola))   
		{	 
			bola.rebotarHorizontal();
			vidas--;
			this.LadrilloRoto();
		}
		
		else if (this.contactoLatDer(bola))   
		{	 
			bola.rebotarHorizontal();
			vidas--;
			this.LadrilloRoto();

		}
	}
	
	public void mostrar(Entorno ent)
	{
		ent.dibujarImagen(ladrillo, x, y, 0, 1);
	}
	
	//Metodos Privados
	
	private boolean azar()
	{
		Random generador = new Random();
		
		int X = (int)(generador.nextDouble() * 6 + 1);
		
		if (X==1)	
			return true;
		
		else
			return false;
	}
	
	private boolean contactoEnTecho(Bola bola)
	{
		int lateralIzq = this.getVerticeIzquierdo();
		int lateralDer = this.getVerticeDerecho();
		int techo = this.getTecho();
		int piso = this.getPiso();
		int xBola = bola.getX();
		int yBola = bola.getY() - bola.getRadio();
		
		return (lateralIzq < xBola && xBola < lateralDer && techo <= yBola && yBola < piso);
	}
	
	private boolean contactoEnPiso(Bola bola)
	{
		int lateralIzq = this.getVerticeIzquierdo();
		int lateralDer = this.getVerticeDerecho();
		int techo = this.getTecho();
		int piso = this.getPiso();
		int xBola = bola.getX();
		int yBola = bola.getY() - bola.getRadio();
		
		return (lateralIzq < xBola && xBola < lateralDer && piso >= yBola && yBola > techo);
	}
	
	private boolean contactoLatIzq(Bola bola)
	{
		int techo = this.getTecho();
		int piso = this.getPiso();
		int lateralIzq = this.getVerticeIzquierdo();
		int lateralDer = this.getVerticeDerecho();
		int xBola = bola.getX() + bola.getRadio();
		int yBola = bola.getY();
		
		return (techo <= yBola && yBola <= piso && lateralIzq <= xBola && xBola < lateralDer);
	}
	
	private boolean contactoLatDer(Bola bola)
	{
		int techo = this.getTecho();
		int piso = this.getPiso();
		int lateralIzq = this.getVerticeIzquierdo();
		int lateralDer = this.getVerticeDerecho();
		int xBola = bola.getX() - bola.getRadio();
		int yBola = bola.getY();
		
		return (techo <= yBola && yBola <= piso && lateralDer >= xBola && xBola > lateralIzq);
	}
	
	private void LadrilloRoto()
	{
		if (this.vidas==1)
			this.ladrillo = Herramientas.cargarImagen("ladrillo_duro_roto_fondo_transparente.png");
	}
}
