package juego;
import entorno.Entorno;

import java.awt.Color;

public class Bola
{
	private int x,y,vx,vy;
	private double diametro;
	public boolean mover;
	
	public Bola()
	{
		this.x = 200;
		this.y = 530;
		this.diametro = 16;
		this.vx = 3;
		this.vy = 3;
		this.mover = true;
	}
	
	//Geterts
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public int getVx()
	{
		return this.vx;
	}
	
	public int getVy()
	{
		return this.vy;
	}
	
	public int getRadio()
	{
		return (int) (this.diametro/2);
	}
	
	//Seters
	
	public void setX(int x)
	{
		this.x = x;
	}
	
	public void setY(int y)
	{
		this.y = y;
	}
	
	public void setVX(int x)
	{
		this.vx = x;
	}
	
	public void setVY(int y)
	{
		this.vy = y;
	}
	
	//Metodos publicos
	
	public void moverse(Entorno entorno) 
	{
		if (mover)
		{
			this.x += vx;
			this.y -= vy;
			
			if (this.getY() - this.getRadio() < 0)
				this.rebotarVertical();
				
			if ((this.getX() - this.getRadio() < 0 || this.getX() + this.getRadio()> (entorno.ancho()-250)))
				this.rebotarHorizontal();		
		}
	}
	
	public void rebotarHorizontal() 
	{
		this.vx = -this.vx;
	}

	public void rebotarVertical() 
	{
		this.vy = -this.vy;
	}
	
	public void ponerSobreBarrita (Barrita barra)
	{
		this.setX(barra.getX());
		this.setY(barra.getTecho()-3); //le agruegue un -3 para que quede afuera del rebotar de la barra
	}
	
	public boolean seFuePorAbajo(Entorno ent)
	{
		return (this.y - this.getRadio() > ent.alto());
	}
	
	public void mostrar(Entorno ent)
	{
		ent.dibujarCirculo(this.x, this.y, this.diametro, Color.white);
	}
	
	public void enlentecer()
	{
		int v = 2;
		
		if (this.getVx()>=0 && this.getVy()>=0)
		{
			this.setVX(v);
			this.setVY(v);
		}
		
		else if ((this.getVx()<=0 && this.getVy()<=0))
		{
			this.setVX(-v);
			this.setVY(-v);
		}
		
		else if ((this.getVx()<=0 && this.getVy()>=0))
		{
			this.setVX(-v);
			this.setVY(v);
		}
		
		else if ((this.getVx()>=0 && this.getVy()<=0))
		{
			this.setVX(v);
			this.setVY(-v);
		}
	}
	
	public void acelerar()
	{
		int v = 4;
		
		if (this.getVx()>=0 && this.getVy()>=0)
		{
			this.setVX(v);
			this.setVY(v);
		}
		
		else if ((this.getVx()<=0 && this.getVy()<=0))
		{
			this.setVX(-v);
			this.setVY(-v);
		}
		
		else if ((this.getVx()<=0 && this.getVy()>=0))
		{
			this.setVX(-v);
			this.setVY(v);
		}
		
		else if ((this.getVx()>=0 && this.getVy()<=0))
		{
			this.setVX(v);
			this.setVY(-v);
		}
	}
	
	public void restablecerBola()
	{
		int v = 3;
		
		if (this.getVx()>=0 && this.getVy()>=0)
		{
			this.setVX(v);
			this.setVY(v);
		}
		
		else if ((this.getVx()<=0 && this.getVy()<=0))
		{
			this.setVX(-v);
			this.setVY(-v);
		}
		
		else if ((this.getVx()<=0 && this.getVy()>=0))
		{
			this.setVX(-v);
			this.setVY(v);
		}
		
		else if ((this.getVx()>=0 && this.getVy()<=0))
		{
			this.setVX(v);
			this.setVY(-v);
		}
	}

	//Metodos para bala
	
	public void mostrarBala(Entorno ent)
	{
		ent.dibujarRectangulo(this.x,this.y, 8, 8, 0, Color.white);
	}
	
	public void moverBala()
	{
		this.y -= vy;
	}
}