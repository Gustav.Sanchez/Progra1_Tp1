package juego;

import java.awt.Color;
import java.util.Random;

import entorno.Entorno;

public class Paquete 
{
	private int x,y,alto,ancho,tipo,vy;
	private boolean agarrado,usado;
	
	public Paquete(Ladrillo ladrillo)
	{
		this.tipo = azar();
		this.x = ladrillo.getX();
		this.y = ladrillo.getY();
		this.alto = 10;
		this.ancho = 10;
		this.vy = 2;
		this.agarrado = false;
		this.usado = false;
	}
	
	//Geters
	
	public void getPoder(Bola bola, Barrita barra, Jugador jugador)
	{
		if (this.tipo == 1)
			barra.alargar();
		
		else if (this.tipo == 2)
			bola.enlentecer();
		
		else if (this.tipo == 3)
			bola.acelerar();
		
		else if (this.tipo == 4)
			jugador.ganaUnaVida();
		
		else if (this.tipo == 5)
			barra.empegotar();
		
		else if (this.tipo == 6)
			barra.armar();
	}
	
	public boolean getPaqueteusado()
	{
		return this.usado;
	}
	
	public boolean getPaqueteAgarrado()
	{
		return this.agarrado;
	}

	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	//Seters
	
	public void setX(int x)
	{
		this.x = x;
	}
	
	public void setPaqueteAgarrado(boolean x)
	{
		this.agarrado = x;
	}
	
	public void setPaqueteUsado(boolean x)
	{
		this.usado = x;
	}
	
	//Metodos Publicos
	
	public void mover(Entorno entorno)
	{	
		if (this.y <= entorno.alto() + this.alto/2)
			this.y += vy; 
	}
	
	public void mostrar(Entorno ent)
	{
		ent.dibujarRectangulo(this.x,this.y, this.ancho, this.alto, 0, Color.yellow);
	}

	//Metodos Privados
	
	private int azar()
	{
		Random generador = new Random();		
		return (int)(generador.nextDouble() * 6 + 1);
	}
	
}
