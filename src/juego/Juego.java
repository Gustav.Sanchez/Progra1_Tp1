package juego;


import java.awt.Color;

//import java.awt.Color;

import entorno.Entorno;
import entorno.InterfaceJuego;


public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	
	//variables
	Barrita barra = new Barrita();
	Bola bola = new Bola();
	Jugador jugador = new Jugador();
	Pared pared = new Pared();
	PaquetesDelJuego paquete = new PaquetesDelJuego(pared);
	Bala balas = new Bala(barra);
	int disparos = -2;
	boolean empezarJuego = false;
	boolean equipado = false;
	boolean juegoPerdido = false;
	boolean juegoGanado = false;
	boolean modoTrampa = false;
	boolean reiniciarContador = true;
	int contAlargar = 0;
	int contAcelerar = 0;
	int contEnlentecer = 0;
	
	
	
	// Variables y métodos propios de cada grupo
	// ...
	
	 void inicio()
	 {
		 if (!empezarJuego && !juegoPerdido && !juegoGanado)
		 {
			 barra.moverBarra(entorno);
			 bola.ponerSobreBarrita(barra);
			 pared.mostrar(entorno);
			 barra.mostrar(entorno);	
			 bola.mostrar(entorno);
			 paquete.mostrar(entorno);
			 jugador.mostrarTexto(entorno);
			 jugador.dibujarPared(entorno);
			 restablecerJuego();
			 paquete.borrarPaquetesEnPantalla();
		 }
 
	 }
	 
	 void jugar()
	 {
		 if (empezarJuego && !juegoPerdido && !juegoGanado)
		 {
			 barra.moverBarra(entorno);
			 bola.moverse(entorno);
			 pared.verificarColision(bola);
			 barra.colisionConBola(bola,entorno);
			 pared.mostrar(entorno);
			 barra.mostrar(entorno);	
			 bola.mostrar(entorno);
			 paquete.colisionConBarra(barra);
			 paquete.verificarVisibles(pared);
			 paquete.verificarPoder(barra, bola, jugador);
			 paquete.mover(entorno);
			 paquete.mostrar(entorno);
			 jugador.mostrarTexto(entorno);
			 jugador.dibujarPared(entorno);
			 
		 }
	 }
	 
	 void juegoTerminado()
	 {
		 if (juegoPerdido)
		 {
			 bola.mostrar(entorno);
			 pared.mostrar(entorno);
			 barra.mostrar(entorno);
			 jugador.mostrarTexto(entorno);
			 jugador.dibujarPared(entorno);
			 entorno.escribirTexto("Juego Perdido", (entorno.ancho()-314)/2, 300);
			 
		 }
		 else if (juegoGanado)
		 {
			 bola.ponerSobreBarrita(barra);
			 bola.mostrar(entorno);
			 pared.mostrar(entorno);
			 barra.mostrar(entorno);
			 jugador.mostrarTexto(entorno);
			 jugador.dibujarPared(entorno);
			 entorno.escribirTexto("Has Ganado!!", (entorno.ancho()-314)/2, 300);
		 }
	 }
	 
	 void contadorDisparos()
	 {
		 if (barra.getArmado() && reiniciarContador)
		 {
			 disparos= -2;
			 reiniciarContador = false;
		 }
			if (entorno.sePresiono(entorno.TECLA_ESPACIO))
			{
				disparos+=2;
				if(disparos>=balas.cantidadDeBalas())
					disparos=-2;
		    }				 
	 }

	 void poderEquiparBarra()
	 {
		 if (barra.getArmado())
		 {
			 contadorDisparos();
			 balas.hacerVisibles(disparos);
			 balas.ponerBalasSobreBarra(barra,disparos);
			 balas.moverBalas();
			 balas.mostrarBalas(entorno);
			 balas.colisionConPared(pared);
		 }
			 if (balas.sinBalas())
			 {
				 barra.desarmar();
				 reiniciarContador = true;
				 balas = new Bala(barra);
			 }  
	 }
	 
	 void controlDelJuego()
	 {
		 if (entorno.sePresiono(entorno.TECLA_ENTER))
			 empezarJuego = true;

		 if (entorno.sePresiono(entorno.TECLA_SHIFT))
			 modoTrampa = true;
		 
		 if (bola.seFuePorAbajo(entorno))
		 {
			 jugador.pierdeUnaVida();
			 empezarJuego = false;
		 }
		 
		 else if (jugador.getVidas()==0)
			 juegoPerdido = true;
		 
		 else if (pared.cantidadDeLadrillosVisibles()==0)
			 juegoGanado = true;
		 
		 juegoTerminado();
	 }
	 
	 void modoTrampa()
	 {
		 
		 if (modoTrampa)
		 {
			 entorno.cambiarFont("arial", 12, Color.CYAN);
			 entorno.escribirTexto("Chuck Norris: "+"ON", entorno.ancho()-220, 150);
			 
			 if (entorno.sePresiono(entorno.TECLA_INICIO))
				 barra.alargar();
			 
			 else if (entorno.sePresiono(entorno.TECLA_FIN))
				 bola.acelerar();
			 
			 else if (entorno.sePresiono(entorno.TECLA_INSERT))
				 bola.enlentecer();
			 
			 else if (entorno.sePresiono(entorno.TECLA_DELETE))
				 jugador.ganaUnaVida();
				 
			 else if (entorno.sePresiono(entorno.TECLA_ARRIBA))
				barra.empegotar();
			 
			 else if (entorno.sePresiono(entorno.TECLA_ABAJO))
				barra.armar();

			 else if (entorno.sePresiono(entorno.TECLA_CTRL))
				 restablecerJuego();
	
		 }
		 else
			 {
			 	entorno.cambiarFont("arial", 12, Color.RED);
			 	entorno.escribirTexto("Chuck Norris: "+"OFF", entorno.ancho()-220, 150);
			 }
	 }
	 
	 void contadores()
	 {
		 if (barra.getAncho() != 100)
		 {
			 contAlargar++;
			 
			 if(contAlargar>300)
			 {
				 barra.restablecerBarra();
				 contAlargar = 0;
			 }
				 
		 }
		 
		 if (bola.getVx()>3 && bola.getVy()>3)
		 {
			 contAcelerar++;
			 
			 if (contAcelerar>300)
			 {
				 bola.restablecerBola();
				 contAcelerar = 0;
			 }
		 }
		 
		 if (bola.getVx()<3 && bola.getVy()<3)
		 {
			 contEnlentecer++;
			 
			 if (contEnlentecer>300)
			 {
				 bola.restablecerBola();
				 contEnlentecer = 0;
			 }
		 }
	 }
	 
	 void restablecerJuego()
	 {
		 barra.restablecerBarra();
		 bola.restablecerBola();
		 barra.desarmar();
		 barra.despegotar();
	 }
	 
	 
	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "ArkaUngs - Grupo 10: Sanchez - Ortiz - Robutti - V1.0", 800, 600);
		
		
		// Inicializar lo que haga falta para el juego
		// ...
		
		barra.setX((entorno.ancho() - 225)/2);  //Se ubica la barra en el medio de la pantalla que se ultiza para el juego.

		

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick()
	{
		// Procesamiento de un instante de tiempo
		// ...
		
		controlDelJuego();
		inicio();
		jugar();	
		poderEquiparBarra();
		modoTrampa();
		contadores();	
	}
	

	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
	 

}
