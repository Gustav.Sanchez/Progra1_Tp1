package juego;
import entorno.Entorno;

public class Bala 

{	
	private Bola[] balas = new Bola[20];
	private Boolean[] visibles = new Boolean[20];
	private Boolean[] usado = new Boolean[20];
	private Boolean[] seteado = new Boolean[20];
	
	public Bala(Barrita barra)
	{
		for (int i = 0; i< balas.length; i++)
		{
			balas[i] = new Bola();
			balas[i].setY(barra.getTecho());
			visibles[i] = false;
			usado[i] = false;
			seteado[i]=false;
		}
	}
	
	//Metodos publicos
	
	public void mostrarBalas(Entorno ent)
	{
		for (int i = 0; i< balas.length; i++)
		{
			if(visibles[i] && !usado[i])
			{
				balas[i].mostrarBala(ent);	
			}
		}
	}
	
	public void moverBalas()
	{
		for (int i = 0; i< balas.length; i++)
		{
			if(visibles[i] && !usado[i])
				balas[i].moverBala();
		}
	}
	
	public void hacerVisibles(int i)
	{
		if (i>=0)
		{
			visibles[i] =true;
			visibles[i+1] = true;	
		}	
	}
	
	public void ponerBalasSobreBarra(Barrita barra,int i)
	{
		if (i>=0)
		{
			if(!seteado[i] && !seteado[i+1])
			{
				balas[i].setX(barra.getVerticeIzquierdo()+20);
				balas[i+1].setX(barra.getVerticeDerecho()-20);
				seteado[i]=true;
				seteado[i+1]=true;	
			}
		}
	}
	
	public void colisionConPared(Pared p)
	{
		for (int i=0; i < this.balas.length; i++)
		{
			p.verificarColision(this.balas[i]);
			
			if(this.balas[i].getVy()<0)
			{
				this.visibles[i] = false;
				this.usado[i] = true;	
			}
		}
	}
	
	public boolean sinBalas()
	{
		int balasUsadas = 0;
		
		for (int i=0; i < this.balas.length; i++)
		{	
			if (this.usado[i])
				balasUsadas++;
		}
		
		return (balasUsadas == usado.length);
	}
	
	public int cantidadDeBalas()
	{
		return this.balas.length;
	}
}
