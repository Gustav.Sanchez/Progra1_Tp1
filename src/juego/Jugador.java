package juego;

import java.awt.Color;

import entorno.Entorno;

public class Jugador 
{
	private int vidas;
	
	public Jugador()
	{
		this.vidas = 3;
	}
	
	//Geters
	
	public int getVidas()
	{
		return this.vidas;
	}
	
	//Seters
	
	public void ganaUnaVida()
	{
		this.vidas++;
	}
	
	public void pierdeUnaVida()
	{
		this.vidas--;
	}
	
	//Metodos Publicos
	
	public void mostrarTexto(Entorno entorno)
	{
		entorno.cambiarFont("arial", 12, Color.CYAN);
		entorno.escribirTexto("Vidas restantes: "+ this.getVidas(), entorno.ancho()-220, 30);
		entorno.escribirTexto("Toque Shift para modo Chuck Norris", entorno.ancho()-220, 90);
		entorno.escribirTexto("Presione inicio para Alargar", entorno.ancho()-220, 170);
		entorno.escribirTexto("Presione Insert para Enlentecer", entorno.ancho()-220, 190);
		entorno.escribirTexto("Presione Fin para Acelerar", entorno.ancho()-220, 210);
		entorno.escribirTexto("Presione Supr para + Vida", entorno.ancho()-220, 230);
		entorno.escribirTexto("Presione Flecha Arriba para Pegote", entorno.ancho()-220, 250);
		entorno.escribirTexto("Presione Flecha Abajo para Equipar", entorno.ancho()-220, 270);
		entorno.escribirTexto("Presione Ctrl para Restablecer", entorno.ancho()-220, 290);
	}
	
	public void dibujarPared(Entorno entorno)
	{
		entorno.dibujarRectangulo(entorno.ancho()-240, entorno.alto()/2, 10, entorno.alto(), 0, Color.ORANGE);
		entorno.dibujarRectangulo(entorno.ancho()-120, 5, 240, 10, 0, Color.ORANGE);
		entorno.dibujarRectangulo(entorno.ancho()-5, entorno.alto()/2, 10, entorno.alto(), 0, Color.ORANGE);
	}
}