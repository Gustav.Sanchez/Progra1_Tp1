package juego;
import entorno.Entorno;

import java.awt.Color;


public class Barrita 
{
	private int x,y,alto,ancho,distancia;
	private boolean pegote,armado;
	
	public Barrita()
	{
		this.x = 200;
		this.y = 550;
		this.alto = 20;
		this.ancho = 100;
		this.pegote = false;
		this.armado = false;
	}
	
	//Geters
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public int getAncho()
	{
		return this.ancho;
	}
	
	public int getAlto()
	{
		return this.alto;
	}
	
	public int getVerticeIzquierdo()
	{
		return this.getX()-(this.getAncho()/2);
	}
	
	public int getVerticeDerecho()
	{
		return this.getX()+(this.getAncho()/2);
	}
	
	public int getTecho()
	{
		return this.getY() - this.getAlto();
	}
	
	public int getPiso()
	{
		return this.getY() + this.getAlto();
	}
	
	public boolean getArmado()
	{
		return this.armado;
	}
	
	//seters
	
	public void setX(int x)
	{
		this.x = x ;
	}
	
	public void setAncho(int x)
	{	
			this.ancho = x;
	}
	
	public void desarmar()
	{
		this.armado = false;
	}
	
	public void armar()
	{
		this.armado = true;
	}
	
	public void alargar()
	{
		this.setAncho(180);
	}
	
	public void restablecerBarra()
	{
		this.setAncho(100);
	}
	
	public void despegotar()
	{
		this.pegote = false;
	}
	
	public void empegotar()
	{
		this.pegote = true;
	}
	
	//Metodos Publicos
	
	
	public void moverBarra(Entorno entorno)
	{
		 
		if (this.getVerticeIzquierdo()<=0)
			this.setX(this.getAncho()/2);
		
		if (getVerticeDerecho()>entorno.ancho()-250)
			this.setX(entorno.ancho()-250-this.getAncho()/2);
			
		if (entorno.estaPresionada(entorno.TECLA_DERECHA) && !(this.getVerticeDerecho()==entorno.ancho()-250))
			this.setX(this.getX()+10);
			
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && !(this.getVerticeIzquierdo()==0))
			this.setX(this.getX()-10);
	}
		 
	public void colisionConBola(Bola bola, Entorno entorno)
	 {	
		if(!this.pegote)
		{
			if (this.contactoEnTecho1(bola))
				bola.rebotarVertical();	

			else if (this.contactoLatIzq(bola)) 
				bola.rebotarHorizontal();

			else if (this.contactoLatDer(bola))  
				bola.rebotarHorizontal();	
		}
		
		else if (this.contactoEnTecho2(bola))
			{
				bola.mover = false;
				bola.setVX(3);
				bola.setVY(3);
				this.moverBola(bola, entorno);
				int n = this.getX()-bola.getX();
				this.distancia = Math.abs(n);
			}
					
		else if (this.contactoLatIzq(bola)) 
				bola.rebotarHorizontal();

		else if (this.contactoLatDer(bola))   
				bola.rebotarHorizontal();	
		
		if (entorno.sePresiono(entorno.TECLA_ESPACIO))
		{
			this.pegote = false;
			bola.mover = true;
		}
	 }
	
	public void moverBola(Bola bola, Entorno entorno)
	{	
		if (this.getVerticeIzquierdo()<=0)
		{
			if (bola.getX()<=this.getX())
				bola.setX(this.getX()-distancia);
		
			if (bola.getX()>this.getX())
				bola.setX(this.getX()+distancia);
		}
		
		if (this.getVerticeDerecho()>=entorno.ancho()-250)
		{
			if (bola.getX()<=this.getX())
				bola.setX(this.getX()-distancia);
	
			if (bola.getX()>this.getX())
				bola.setX(this.getX()+distancia);
		}
		
		if (entorno.estaPresionada(entorno.TECLA_DERECHA) && !(this.getVerticeDerecho()==entorno.ancho()-250))
			bola.setX(bola.getX()+10);
			
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && !(this.getVerticeIzquierdo()==0))
			bola.setX(bola.getX()-10);
	}
	
	public void mostrar(Entorno ent)
	{
		ent.dibujarRectangulo(this.x,this.y, this.ancho, this.alto, 0, Color.blue);
	}
	
	//Metodos privados
	
	private boolean contactoEnTecho1(Bola bola)
	{
		int lateralIzq = this.getVerticeIzquierdo();
		int lateralDer = this.getVerticeDerecho();
		int techo1 = this.getTecho()+1;
		int piso = this.getPiso();
		int xBola = bola.getX();
		int yBola = bola.getY() + (bola.getRadio()/2);
		
		return (lateralIzq <= xBola && xBola <= lateralDer && techo1 <= yBola && yBola < piso);
	}
	
	private boolean contactoEnTecho2(Bola bola)
	{
		int lateralIzq = this.getVerticeIzquierdo();
		int lateralDer = this.getVerticeDerecho();
		int techo2 = this.getTecho()-1;
		int piso = this.getPiso();
		int xBola = bola.getX();
		int yBola = bola.getY() + (bola.getRadio()/2);
		
		return (lateralIzq <= xBola && xBola <= lateralDer && techo2 <= yBola && yBola < piso);
	}
	
	private boolean contactoLatIzq(Bola bola)
	{
		int lateralIzq = this.getVerticeIzquierdo();
		int lateralDer = this.getVerticeDerecho();
		int techo = this.getTecho();
		int piso = this.getPiso();
		int xBola = bola.getX() + bola.getRadio();
		int yBola = bola.getY();
		
		return (techo < yBola && yBola < piso && lateralIzq <= xBola && xBola < lateralDer);
	}

	private boolean contactoLatDer(Bola bola)
	{
		int lateralIzq = this.getVerticeIzquierdo();
		int lateralDer = this.getVerticeDerecho();
		int techo = this.getTecho();
		int piso = this.getPiso();
		int xBola = bola.getX() - bola.getRadio();
		int yBola = bola.getY();
		
		return (techo < yBola && yBola < piso && lateralDer >= xBola && xBola > lateralIzq); 
	}	
}